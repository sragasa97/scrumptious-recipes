# Generated by Django 4.1.5 on 2023-01-17 22:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0007_alter_recipe_title_recipeingredient'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipeingredient',
            name='recipe',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Ingredients', to='recipes.recipe'),
        ),
        migrations.AlterField(
            model_name='recipestep',
            name='recipe',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Steps', to='recipes.recipe'),
        ),
    ]
