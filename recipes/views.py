from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from .models import Recipe
from .forms import RecipeForm

def show_recipe(request, id):
    a_really_cool_recipe = get_object_or_404(Recipe, id=id)
    recipes = Recipe.objects.all()
    context = {
         "recipe_object": a_really_cool_recipe,
         "recipe_list": recipes,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
    context = {
        "form": form,
    }
    return render(request, "recipes/create.html", context)

@login_required
def edit_recipe(request, id):
    recipes_to_edit = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipes_to_edit)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(request.GET, instance=recipes_to_edit)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)

    context = {
        "post": recipes_to_edit,
        "form": form,
    }
    return render(request, "recipes/edit.html", context)
