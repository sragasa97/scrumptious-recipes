from django.contrib import admin
from .models import Recipe, RecipeStep, RecipeIngredient

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title", "id", "description", "gluten_free", "rating", "prep_time", "created_on", "picture",
    )

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "recipe_id", "step_number", "instruction",
    )

@admin.register(RecipeIngredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display = (
        "recipe_id", "amount", "food_item",
    )
