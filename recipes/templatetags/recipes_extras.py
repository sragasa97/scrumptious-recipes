from django import template

register = template.Library()

@register.filter(name="multiply")
def multiply(string, number):
    return string * number
