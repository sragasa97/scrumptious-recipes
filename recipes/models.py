from django.db import models
from django.conf import settings

class Recipe(models.Model):
    title = models.CharField(max_length=200, blank=True)
    picture = models.URLField(blank=True)
    description = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    gluten_free = models.BooleanField(null=True, blank=True)
    rating = models.PositiveSmallIntegerField(blank=True)
    prep_time = models.CharField(max_length=30, blank=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name = "recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title

class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField(blank=True)
    recipe = models.ForeignKey(
        Recipe,
        related_name="Steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["recipe_id"]

    def __str__(self):
        return self.recipe.title

class RecipeIngredient(models.Model):
    amount = models.CharField(max_length=200, blank=True)
    food_item = models.CharField(max_length=200, blank=True)
    recipe = models.ForeignKey(
        Recipe,
        related_name="Ingredients",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["food_item"]

    def __str__(self):
        return self.recipe.title
